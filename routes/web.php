<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::get('/', function () { return view('welcome'); });

/* Front End */
Route::get('/','HomeController@index')->name('main');

Route::get('person','PersonController@index')->name('person');
Route::get('report','ReportController@index')->name('report');
Route::get('resolution','ResolController@index')->name('resolution');

// Back End
Route::get('backoffice','BackofficeController@index')->name('backoffice');

Route::get('committee','BackofficeController@committee')->name('committee');

Route::get('committee/new','BackofficeController@frmPerson')->name('addPerson');
Route::get('committee/store','PersonController@store')->name('storePerson');
Route::resource('/FrmEditPerson','PersonController');
Route::resource('/manage_person', 'PersonController');


Route::get('secretary','BackofficeController@secretary')->name('secretary');

Route::get('content','BackofficeController@content')->name('content');
Route::get('content/new','BackofficeController@frmContent')->name('addContent');
Route::resource('/manage_content', 'ContentController');
Route::resource('/frmEditContent','ContentController');
Route::resource('/editContent','ContentController');

Route::get('meeting','BackofficeController@meeting')->name('meeting');







####test
Route::get('/uploadfile','UploadFileController@index');
Route::post('/uploadfile','UploadFileController@showUploadFile');
####test

