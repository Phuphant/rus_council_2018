let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');



/* Front END */
mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
],'public/js/all.js');

mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',    
    'public/css/business-frontpage.css',
],'public/css/all.css');
/* Front END */

/* Back END */
mix.styles([    
    'node_modules/bootstrap/dist/css/bootstrap.min.css',       
    'node_modules/font-awesome/scss/font-awesome.scss',
    'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css',        
    'public/css/sb-admin.css',
],'public/css/all_backend.css');

mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
    'node_modules/jquery-easing/jquery.easing.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.js',
    'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
    'public/js/sb-admin.css',
    'public/js/demo/datatables-demo.js',
],'public/js/all_backend.js');
/* Back END */