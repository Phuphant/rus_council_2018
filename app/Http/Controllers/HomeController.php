<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class HomeController extends Controller{
    public function index(){ 

        $resource["Content_Show_Hilight"]   =  App\vw_content::where('CategoryCode','pr')
                                                ->where('ContentHilight','yes')
                                                ->orderby('ContentID','desc')
                                                ->take(3)
                                                ->get();

        $resource["Content_Showall"]        = App\vw_content::where('CategoryCode','pr')
                                                ->where('ContentHilight',null)                                                
                                                ->orderby('ContentID','desc')
                                                ->take(6)
                                                ->get();

        $resource["president_of_council"]   = App\vw_his_person::where('pos_counc_id','1')    
                                                ->get();

        return view('main',$resource);
    }
}
