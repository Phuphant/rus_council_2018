<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ReportController extends Controller{
    public function index(){
        $resource['DataContent'] =   App\vw_content::where('CategoryCode','meeting')
                                    ->orderBy('created_at', 'desc')
                                    ->get();
        
        $resource['DataYears']    =  App\tb_year::orderby('id','DESC')->get();

        return view('report_meet',$resource);
    }
}
