<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ResolController extends Controller{
    public function index(){
        $resource['category']   = App\categoty::where('CategoryStatus','on')
                                ->where('CategoryDelete','resolution')                                
                                ->get(); 

        $resource['DataContent'] =   App\vw_content::where('CategoryDelete','resolution')
                                ->orderBy('created_at', 'desc')
                                ->get();                                

        $resource['DataYears']  =  App\tb_year::orderby('id','DESC')                                                     
                                ->get();
        
        return view('resolution',$resource);
    }
}
