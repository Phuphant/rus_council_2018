<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;


class BackofficeController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('backend.index');
    }

    public function committee(){
        $resource['DataHisPerson'] =    App\vw_his_person::where('pos_counc_id','<>',7)
                                        ->get();

        $resource['topicPage'] = "ข้อมูลกรรมการสภามหาวิทยาลัยฯ";

        return view('backend.person',$resource);
    }

    public function secretary(){
        $resource['DataHisPerson'] =    App\vw_his_person::where('pos_counc_id','=',7)
                                        ->get();

        $resource['topicPage'] = "ข้อมูลเลขาสภามหาวิทยาลัยฯ";

        return view('backend.person',$resource);
    }

    public function content(){
        $resource['DataContent'] =      App\vw_content::where('CategoryCode','pr')
                                        ->orderBy('created_at', 'desc')
                                        ->get();

        $resource['topicPage'] = "ข้อมูลประชาสัมพันธ์";

        return view('backend.content',$resource);
    }

    public function meeting(){
        $resource['DataContent'] =   App\vw_content::where('CategoryCode','meeting')
                                        ->orderBy('created_at', 'desc')
                                        ->get();                                                

        $resource['topicPage'] = "ข้อมูลรายงานการประชุม";

        return view('backend.content',$resource);
    }

   
    
    public function frmPerson(){
        $resource['title_name'] =   App\titlenameModel::where('record_status','N')
                                    ->get();

        $resource['province'] = App\provinceModel::all();
        $resource['amphur'] = App\amphurModel::all();
        $resource['district'] = App\districtModel::all();

        $resource['posCouncil'] =   App\posCounncilModel::where('record_status','N')
                                    ->get();
        
        $resource['posAcad'] =  App\posAcadModel::where('record_status','N')
                                ->get();
                            
        $resource['posExec'] =  App\posExecModel::where('record_status','N')
                                ->get();                                
                                    
        $resource['topicPage'] = "เพิมรายชื่อบุคคล";

        return view('backend.frm_person',$resource);        
    }

    public function frmContent(){
                                    
        $resource['category'] =  App\categoty::where('CategoryStatus','on')->get();      
        $resource['year']       = App\tb_year::all();                            
                                    
        $resource['topicPage'] = "เพิมรายการข่าวประชาสัมพันธ์";

        return view('backend.frm_content',$resource);        
    }
    
}
