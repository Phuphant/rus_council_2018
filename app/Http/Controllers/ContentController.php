<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $content = new \App\MN_content();    
        $content->ContentID         = "content".date("YmdHis");    
        $content->CategoryID        = $request->txt_CategoruID;
        $content->ContentName       = $request->txt_ContentName;
        $content->ContentDetail     = $request->txt_ContentDetail;
        $content->ContentPic        = $request->txt_ContentPic;
        $content->ContentLink       = $request->txt_ContentLink;
        $content->ContentIcon       = $request->chkIcon;        
        $content->ContentLinkURL    = $request->txt_ContentLinkURL;
        $content->ContentEXTLink    = $request->chkEXTLink;
        $content->ContentHilight    = $request->chkHilight;
        $content->ContentStatus     = $request->txt_ContentStatus;
        $content->ContentOwner      = "-";
        $content->ContentTags       = "-";
        $content->ContentLinkGAL    = "-";
        $content->ContentYear       = $request->txt_ContentYear;

        #return $content;
        $content->save();       
        return redirect()->route('content')->with('feedback', 'บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $resource['id']         = $id;
        $resource['content']    = App\vw_content::where('ContentID',$id)->get();     
        $resource['category']   = App\categoty::where('CategoryStatus','on')->get();  
        $resource['year']       = App\tb_year::all();  

        $resource['topicPage']  = "แก้ไขข้อมูลเนื้อหาเว็บไซต์";

        return view('backend.frm_content_edit',$resource);           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        /*
        return $request->txt_ContentLinkURL;;
        if($request->hasFile('txt_ContentLink')){
            return $request->txt_ContentLink;
        }
        /* */
        /*
        $content = \App\MN_content::where('ContentID',$id)->get();  
        //return $content;
        //$content->ContentID         = $request->txt_ContentID;   
        $content->CategoryID        = $request->txt_CategoruID;
        $content->ContentName       = $request->txt_ContentName;
        $content->ContentDetail     = $request->txt_ContentDetail;
        $content->ContentPic        = $request->txt_ContentPic;
        $content->ContentLink       = $request->txt_ContentLink;
        $content->ContentIcon       = $request->chkIcon;        
        $content->ContentLinkURL    = $request->txt_ContentLinkURL;
        $content->ContentEXTLink    = $request->chkEXTLink;
        $content->ContentHilight    = $request->chkHilight;
        $content->ContentStatus     = $request->txt_ContentStatus;
        
        
        $content->update();
        */
        
        DB::update('
            update tb_content set                 
                CategoryID        = ?,
                ContentName       = ?,
                ContentDetail     = ?,
                ContentPic        = ?,
                ContentLink       = ?,
                ContentIcon       = ?,
                ContentLinkURL    = ?,
                ContentEXTLink    = ?,
                ContentHilight    = ?,
                ContentYear       = ?,
                ContentStatus     = ? 
            WHERE ContentID = ?',
            [   $request->txt_CategoruID, 
                $request->txt_ContentName, 
                $request->txt_ContentDetail, 
                $request->txt_ContentPic, 
                $request->txt_ContentLink,
                $request->chkIcon,
                $request->txt_ContentLinkURL, 
                $request->chkEXTLink,
                $request->chkHilight,
                $request->txt_ContentYear,
                $request->txt_ContentStatus, 
                $request->txt_ContentID  
            ]
        );
        
        #return $request;        
        return Redirect('frmEditContent/'.$id.'/edit')->with('alert','แก้ไขข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
