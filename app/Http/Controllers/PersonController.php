<?php

namespace App\Http\Controllers;

use App; 
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;


class PersonController extends Controller{
    
    public function index(){

        $resource['posCouncil'] =   App\posCounncilModel::where('record_status','N')
                                    ->get();

        $resource['DataHisPerson'] =   App\vw_his_person::where('person_status','on')
                                    ->get();

        return view('person',$resource);
    }
    
    public function create()    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)    {

        $person = new \App\MN_person();        
        $person->title_id       = $request->txt_title_id;
        $person->fname_th       = $request->txt_fname_th;
        $person->lname_th       = $request->txt_lname_th;
        $person->id_card        = $request->txt_id_card;
        $person->pos_acac_id    = $request->txt_posAcad;
        $person->pos_exec_id    = $request->txt_posExec;
        $person->addNum         = $request->txt_addNum;
        $person->addTumbol      = $request->txt_addTombol;
        $person->addAmphur      = $request->txt_addAmphur;
        $person->addProvince    = $request->txt_province;
        $person->addZipcode     = $request->txt_addZipcode;
        $person->tel            = $request->txt_tel;
        $person->mobile         = $request->txt_mobile;
        $person->pos_counc_id   = $request->txt_posCounc;
        $person->delegate_year  = $request->txt_delegate_year;
        $person->person_status  = $request->txt_personStatus;
        $person->record_status = 'N';    

        
        //upload
        #return $request->txt_fname_th;
        
        
        #return $request->all();
        
        #if($request->hasFile('txt_person_picture')){
            $newFileName = str_random(20).'.'.$request->file('txt_person_picture')->getClientOriginalExtension();

            #return $newFileName;
            #$request->file('txt_person_picture')->move(public_path().'/upload/person_picture/',$newFileName);
            $request->file('txt_person_picture')->move(public_path().'/images/upload/',$newFileName);
            //ย่อภาพ
            //Image::make(public_path().'/upload/'.$newFileName)->resize(100,null,function ($constraint){$constraint->aspectRatio();})->save(public_path().'/upload/resize/'.$newFileName);

            $person->person_picture = $newFileName;
        #}else{
        #    $person->person_picture = "nopic.jpg";
        #}
        return $person;
        $person->save();
       # return redirect()->route('manage_person.show',$request->txt_id_card)->with('feedback', 'บันทึกข้อมูลเรียบร้อยแล้ว');
        return redirect()->route('committee')->with('feedback', 'บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $resource['id'] = $id;          
        $resource['person']     = App\MN_person::where('personID',$id)->get();         
        $resource['title_name'] = App\titlenameModel::where('record_status','N')->get();        
        $resource['posCouncil'] = App\posCounncilModel::where('record_status','N')->get();
        $resource['posAcad']    = App\posAcadModel::where('record_status','N')->get();                   
        $resource['posExec']    = App\posExecModel::where('record_status','N')->get();                                
        $resource['province']   = App\provinceModel::all();
        $resource['amphur']     = App\amphurModel::all();
        $resource['district']   = App\districtModel::all();                                    
        $resource['topicPage']  = "แก้ไขรายชื่อบุคคล";
        

        return view('backend.frm_person_edit',$resource);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        #return $request->txt_fname_th;
        DB::update('
            update his_person set                                 
                title_id        = ?,
                fname_th        = ?,
                lname_th        = ?,
                id_card         = ?,
                pos_acac_id     = ?,
                pos_exec_id     = ?,
                person_picture  = ?,
                addNum          = ?,
                addTumbol       = ?,
                addAmphur       = ?,
                addProvince     = ?,
                addZipcode      = ?,
                mobile          = ?,
                tel             = ?,
                pos_counc_id    = ?,
                delegate_year   = ?,
                person_status   = ? 
            WHERE personID = ?',
            [   $request->txt_title_id,
                $request->txt_fname_th, 
                $request->txt_lname_th, 
                $request->txt_id_card, 
                $request->txt_posAcad,
                $request->txt_posExec,
                $request->img, 
                $request->txt_addNum,
                $request->txt_addTombol,
                $request->txt_addAmphur,
                $request->txt_province, 
                $request->txt_addZipcode,
                $request->txt_tel,
                $request->txt_mobile,
                $request->txt_posCounc,
                $request->txt_delegate_year,
                $request->txt_Status,
                $request->txt_PersonID
            ]
        );
        
        #return $request;        
        return Redirect('FrmEditPerson/'.$id.'/edit')->with('alert','แก้ไขข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
