@extends('layout.master')

@section('content')
<!-- Page Content --> 
<div class="container main_font">
  <div class="row">
    <div class="col-sm-12">
      <div class="card pl-2 ">
        {{-- $DataYears --}}
        {{-- TAB --}}        
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            @foreach ($DataYears as $arrYears)
                <li class="nav-item">
                  <a class="nav-link" id="pills-{{ $arrYears->ContentYear }}-tab" data-toggle="pill" href="#pills-{{ $arrYears->ContentYear }}" role="tab" aria-controls="pills-{{ $arrYears->ContentYear }}" aria-selected="true">
                    {{ $arrYears->ContentYear }}
                  </a>
                </li>
            @endforeach
            {{-- 
            <li class="nav-item">
              <a class="nav-link" id="pills-test-tab" data-toggle="pill" href="#pills-test" role="tab" aria-controls="pills-test" aria-selected="true">Home</a>
            </li>   
            --}}           
          </ul>

          <div class="tab-content" id="pills-tabContent">            
            @foreach ($DataYears as $arrYearsDetail)
            <div class="tab-pane fade" id="pills-{{ $arrYearsDetail->ContentYear }}" role="tabpanel" aria-labelledby="pills-{{ $arrYearsDetail->ContentYear }}-tab">              
              <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">                          
                <tbody>
                {{-- $DataContent --}}
                @foreach ($DataContent as $arrContents)
                  @if ($arrContents->ContentYear==$arrYearsDetail->ContentYear)                                  
                    <tr>
                      <td><a href="#">{{ $arrContents->ContentName }}</a></td>
                      <td>-</td>
                    </tr>      
                  @endif
                @endforeach
                </tbody>
              </table>                                          
            </div>
            @endforeach
            {{-- 
            <div class="tab-pane fade" id="pills-test" role="tabpanel" aria-labelledby="pills-test-tab">..test..</div>                       
            --}}
          </div>

      </div>      
    </div>

    <!-- DIV row - container -->
  </div>
</div>
@endsection

