@extends('layout.master')

@section('content')
<!-- Page Content --> 
<div class="container main_font">
  <div class="row">
    <div class="col-sm-12">
      <div class="card pl-2 ">
        
        <!--ta b-->
          <ul class="nav nav-tabs" id="myTab" role="tablist">          
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#person" role="tab" aria-controls="profile" aria-selected="false">กรรมการสภามหาวิทยาลัย</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="contact-tab" data-toggle="tab" href="#secretary" role="tab" aria-controls="contact" aria-selected="false">เลขานุการสภามหาวิทยาลัย</a>
            </li>
          </ul>
          <!--ta b-->

          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="person" role="tabpanel" aria-labelledby="person-tab">
              @include('subView.person1')
            </div>
            <div class="tab-pane fade" id="secretary" role="tabpanel" aria-labelledby="secretary-tab">
              @include('subView.person2')
            </div>            
          </div>

      </div>      
    </div>

    <!-- DIV row - container -->
  </div>
</div>
@endsection

