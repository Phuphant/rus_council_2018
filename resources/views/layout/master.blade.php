<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>RUS | Council</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

  <style>
    .main_font {
      font-family: 'Kanit', sans-serif;
    }

    .card_bg_topic {
      background-color: rgb(250, 185, 35);
    }
  </style>
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top main_font">
    <div class="container">
      <a class="navbar-brand" href="{{route('main')}}">หน้าหลัก</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('person') }}">กรรมการสภา</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('report')}}">รายงานการประชุม</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('resolution') }}">ระเบียบข้อบังคับ</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">คำสั่งสภามหาวิทยาลัย</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">ประกาศมหาวิทยาลัย</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<!-- Header with Background Image -->
  {{-- <header class="business-header" style="margin-bottom: 5px;"> --}}
  <header style="margin-bottom: 5px;">
  <div class="row">
    <div class="col-lg-12">
        <img src="{{ asset('images/rus_council_head.png') }}" width="100%">
    </div>
  </div>  
  {{-- 
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="display-4 text-center text-white mt-4 main_font">สภามหาวิทยาลัยเทคโนโลยีราชมงคลสุวรรณภูมิ</h3>
        </div>
      </div>
    </div>
   --}}
  </header>

@yield('content')

<!-- Footer -->
<footer class="py-5 bg-dark">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
    <p class="m-0 text-center text-white">
      <a href="{{ route('backoffice') }}">Backoffice</a>
    </p>
  </div>
  <!-- /.container -->
</footer>



  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('js/all.js') }}"></script>

</body>

</html>