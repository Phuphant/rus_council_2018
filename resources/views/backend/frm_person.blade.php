@extends('layout.master_backend')

@section('contentBackoffice')
<!-- DataTables Example -->
<div class="card mb-3 main_font">
    <div class="card-header"><i class="fas fa-table"></i>{{ $topicPage }}</div>
    <div class="card-body">
        <div class="table-responsive">


<form  method="POST" action="{{ route('manage_person.store') }}" enctype="multipart/form-data">
  @csrf  
<!-- ข้อมูลส่วนตัว -->    
<div class="card">    
    <div class="card-body">
<div>ข้อมูลส่วนตัว</div>

<!-- title name -->
<div class="form-group mt-3">        
    <select class="form-control" name="txt_title_id">
        <option value="">:: เลือกคำนำหน้า ::</option>
        @foreach ($title_name as $arrTitleName)
            <option value="{{ $arrTitleName->title_id }}">{{ $arrTitleName->title_name_th }}</option>
        @endforeach
    </select>            
</div>   

<!-- name TH -->
<div class="form-group">
<div class="form-row col-md-12">
    <div class="col-md-6">
        <div class="form-label-group">
            <input type="text" name="txt_fname_th" id="firstName_th" class="form-control" placeholder="ระบุชื่อ (ภาษาไทย)"  autofocus="autofocus" required="required">
            <label for="firstName_th">ระบุชื่อ (ภาษาไทย)</label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-label-group">
            <input type="text" name="txt_lname_th" id="lastName_th" class="form-control" placeholder="ระบุนามสกุล (ภาษาไทย)" required="required">
            <label for="lastName_th">ระบุนามสกุล (ภาษาไทย)</label>
        </div>
    </div>
</div>
</div>

<!-- IDcard -->
<div class="form-group">
    <div class="form-label-group">
        <input type="text" name="txt_id_card" id="txt_id_card" class="form-control" placeholder="Email address" >
        <label for="txt_id_card">รหัสบัตรประจำตัวประชาชน</label>
    </div>
</div>

<!-- posAcad -->
<div class="form-group mt-3">        
    <select class="form-control" name="txt_posAcad">
        <option value="">:: เลือกตำแหน่งทางวิชาการ ::</option>
        @foreach ($posAcad as $arrPosAcad)
            <option value="{{ $arrPosAcad->pos_acad_id }}">{{ $arrPosAcad->pos_acad_name_th }}</option>
        @endforeach
    </select>            
</div>

<!-- posExec -->
<div class="form-group mt-3">        
    <select class="form-control" name="txt_posExec">
        <option value="">:: เลือกตำแหน่งทางบริหาร ::</option>
        @foreach ($posExec as $arrPosExec)
            <option value="{{ $arrPosExec->pos_exec_id }}">{{ $arrPosExec->pos_exec_name_th }}</option>
        @endforeach
    </select>            
</div>

<!-- PICTURE -->
<div class="form-group">
        <label for="">ภาพหน้าปก</label>
        <input type="file" class="form-control-file" name="txt_person_picture" id="txt_person_picture" >        
    </div>

    </div> <!--card body-->
</div> <!--card-->

<!-- ข้อมูลการติดต่อ -->    
<div class="card mt-1">    
    <div class="card-body">
<div>ข้อมูลที่อยู่</div>

<!-- ADDRESS -->
<div class="form-group mt-3">        
    <div class="col-md-12">
        <div class="form-label-group">
            <input type="text" name="txt_addNum" id="txt_addNum" class="form-control" placeholder="ที่อยู่เลขที่" autofocus="autofocus">
            <label for="txt_addNum">ที่อยู่เลขที่</label>
        </div>
    </div>              
</div>   

<!-- PROVINCE AMPHUR DISTRICT -->
<div class="form-group">
<div class="form-row col-md-12">
    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_addTombol" id="txt_addTombol" class="form-control" placeholder="ระบุตำบล" autofocus="autofocus">
            <label for="txt_addTombol">ระบุตำบล</label>
        </div>
    </div> 

    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_addAmphur" id="txt_addAmphur" class="form-control" placeholder="ระบุอำเภอ" autofocus="autofocus">
            <label for="txt_addAmphur">ระบุอำเภอ</label>
        </div>
    </div> 
    
    <div class="col-md-4">
    <select class="form-control" name="txt_province">
        <option value="">:: เลือกจังหวัด ::</option>
        @foreach ($province as $arrProvince)
            <option value="{{ $arrProvince->province_id }}">{{ $arrProvince->province_name }}</option>
        @endforeach
    </select> 
    </div>   
</div>
</div>

<!-- ZIPCODE MOBILE TEL -->
<div class="form-group">
<div class="form-row col-md-12">
    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_addZipcode" id="txt_addZipcode" class="form-control" placeholder="ระบุรหัสไปรษณีย์" autofocus="autofocus">
            <label for="txt_addZipcode">ระบุรหัสไปรษณีย์</label>
        </div>
    </div> 

    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_tel" id="txt_tel" class="form-control" placeholder="หมายเลขโทรศัพท์" autofocus="autofocus">
            <label for="txt_tel">หมายเลขโทรศัพท์</label>
        </div>
    </div>   

    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_mobile" id="txt_mobile" class="form-control" placeholder="หมายเลขโทรศัพท์เคลื่อนที่" autofocus="autofocus">
            <label for="txt_mobile">หมายเลขโทรศัพท์เคลื่อนที่</label>
        </div>
    </div>         
</div>
</div>

    </div> <!--card body-->
</div> <!--card-->

<!-- ข้อมูลการดำรงตำแหน่ง -->    
<div class="card mt-1">    
    <div class="card-body">
<div>ข้อมูลการดำรงตำแหน่ง</div>

<!-- POSITION COUNCIL -->
<div class="form-group mt-3">        
    <select class="form-control" name="txt_posCounc" required="required">
        <option value="">:: เลือกตำแหน่งในสภามหาวิทยาลัย ::</option>
        @foreach ($posCouncil as $arrPoscouncil)
            <option value="{{ $arrPoscouncil->pos_counc_id }}">{{ $arrPoscouncil->pos_counc_name_th }}</option>
        @endforeach
    </select>            
</div>   

<!-- DELEGATE YEAR -->
<div class="form-group">
    <div class="form-label-group">
        <input type="text" name="txt_delegate_year" id="txt_delegate_year" class="form-control" placeholder="ปีที่รับตำแหน่ง" >
        <label for="txt_delegate_year">ปีที่รับตำแหน่ง</label>
    </div>
</div>

<!-- PERSON STATUS -->
<div class="form-group mt-3">    
    <label for="txt_personStatus">สถานะการดำรงตำแหน่ง</label>    
    <select class="form-control" name="txt_personStatus" id="txt_personStatus" required="required">
        <option value="">:: เลือกสถานะ ::</option>        
        <option value="on" > ดำรงตำแหน่ง</option>
        <option value="out"> หมดวาระ</option>        
    </select>            
</div>


    </div> <!--card body-->
</div> <!--card-->


<div align="right" class="mt-1">        
    <button type="submit" class="btn btn-primary btn-x">บันทึกข้อมูล</button>
    <a href="#" class="btn btn-secondary btn-x" role="button" aria-pressed="true">ยกเลิก</a>    
</div>

</form>

        </div>
    </div>    
</div>

@endsection