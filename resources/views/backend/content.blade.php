@extends('layout.master_backend')

@section('contentBackoffice')
<!-- DataTables Example -->
<div class="card mb-3 main_font">
    <div class="card-header"><i class="fas fa-table"></i>{{ $topicPage }}</div>
    <div class="card-body">
        <div class="table-responsive">
<div align="right" class="mb-2">
    <a href="{{ route('addContent') }}" class="btn btn-success btn-x active" role="button" aria-pressed="true">เพิ่มรายการ</a>
    <a href="#" class="btn btn-primary btn-x" role="button" aria-pressed="true">บันทึกข้อมูล</a>
    <a href="#" class="btn btn-secondary btn-x" role="button" aria-pressed="true">ลบรายการ</a>    
</div>
        
<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>TOPIC</th>
            <th>ภาพหน้าปก</th>        
            <th>EDIT</th>
            <th>DELETE</th>
            <th>Last Update</th>            
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>ชื่อ-นามสกุล</th>
            <th>ภาพหน้าปก</th>            
            <th>EDIT</th>
            <th>DELETE</th>
            <th>Last Update</th>   
        </tr>
    </tfoot>
    <tbody>
    @foreach ($DataContent as $arrDataContent )            
        <tr>
            <td>
                {{ $arrDataContent->ContentName}}                
            </td>
            <td>
                -
            </td>            
            <td><a href="{{ url('frmEditContent/'.$arrDataContent->ContentID.'/edit') }}" class="btn btn-link">edit</a></td>
            <td>del</td>
            <td>{{ $arrDataContent->updated_at}}</td>            
        </tr>                
    @endforeach        
    </tbody>        
</table>  

        </div>
    </div>    
</div>

@endsection