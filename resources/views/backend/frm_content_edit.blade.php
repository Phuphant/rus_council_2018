@extends('layout.master_backend')

@section('contentBackoffice')
<!-- DataTables Example -->
<div class="card mb-3 main_font">
    <div class="card-header"><i class="fas fa-table"></i>{{ $topicPage }}</div>
    <div class="card-body">
        <div class="table-responsive">

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<form  method="POST" action="{{ url('editContent/'.$id) }}" enctype="multipart/form-data">
  @csrf  
  {{ method_field('PATCH') }}
<!-- ข้อมูล ข่าว -->    
<div class="card">    
    <div class="card-body">

<input type="hidden" name="txt_ContentID" value="{{ $id }}" >
@foreach ($content as $arrContent)
<!-- TOPIC CATEGORY -->
<div class="form-group">      
    <select class="form-control" name="txt_CategoruID" id="txt_CategoruID" required="required">
        <option value="">:: เลือกหมวดหมู่รายการ ::</option>        
        @foreach ($category as $arrCategory )
            <option value="{{ $arrCategory->CategoryID }}" {{ $arrCategory->CategoryID==$arrContent->CategoryID ? 'selected="selected"' : '' }} > 
                {{ $arrCategory->CategoryName }}
            </option>            
        @endforeach     
    </select>            
</div>

<!-- TOPIC -->
<div class="form-group">
    <div class="form-label-group">        
        <input type="text" name="txt_ContentName" id="txt_ContentName" value="{{ $arrContent->ContentName }}" class="form-control" placeholder="หัวข้อรายการ"  required="required">
        <label for="txt_ContentName">หัวข้อรายการ</label>
    </div>
</div>

<!-- DETAIL -->
<div class="form-group">
    <div class="form-label-group">       
        <div>รายละเอียดรายการ</div>
        <textarea rows="4", cols="54" id="txt_ContentDetail" name="txt_ContentDetail" >{{ $arrContent->ContentDetail }}</textarea>        
    </div>
</div>


<!-- PICTURE -->
<div class="form-group">
        <label for="">ภาพหน้าปก</label>
        <input type="file" class="form-control-file" name="txt_ContentPic" id="txt_ContentPic" >        
        เลือกกรณีต้องการแสดงภาพหน้าปก -> <input type="checkbox" name="chkIcon" id="chkIcon" value="yes" {{ $arrContent->ContentIcon=="yes" ? 'checked' : '' }} >
</div>

{{-- ATTECH FILE --}}
<div class="card mt-1">  
        <div class="card-body">
    
    <div>จัดการข้อมูลเอกสารแนบ</div>
    <!-- PICTURE -->
    <div class="form-group">
        <label for="txt_ContentLink">แนบไฟล์</label>
        <input type="file" class="form-control-file" name="txt_ContentLink" id="txt_ContentLink" >            
    </div>
    
    <!-- LINK URL -->
    <div class="form-group">
        <div class="form-label-group">        
            <input type="text" name="txt_ContentLinkURL" id="txt_ContentLinkURL" value="{{ $arrContent->ContentLinkURL }}" class="form-control" placeholder="Link ข้อมูล">
            <label for="txt_ContentLinkURL">Link ข้อมูล</label>
        </div>
    </div>
    
    <!-- EXT LINK -->
    <div class="form-group">
        <div class="form-label-group">        
            <input type="checkbox" name="chkEXTLink" id="chkEXTLink" value="accept" {{ $arrContent->ContentEXTLink=="accept" ? 'checked' : '' }} >
            <label for="chkEXTLink"> ติ๊กเลื่อก เพื่อลิงค์ไปที่ url ตามด้านบน โดยตรง</label>       
        </div>
    </div>
    
        </div>
    </div>
    {{-- ATTECH FILE --}}

    </div> <!--card body-->
</div> <!--card-->

<!-- ตั้งค่า ข้อมูล -->    
<div class="card mt-1">    
    <div class="card-body">

<!-- CONTENT HILIGHT -->
<div class="form-group">
    <div class="form-label-group">        
        ตั้งเป็นหัวข้อ Hilight  -> <input type="checkbox" name="chkHilight" value="yes" id="chkHilight" {{ $arrContent->ContentHilight=="yes" ? 'checked' : '' }} >         
    </div>
</div>

<!-- CONTENT YEAR -->
<div class="form-group">
    <label for="txt_ContentYear" style="color:red;">กรณีรายงานประชุมโปรดระบุปี </label>
    <select class="form-control" name="txt_ContentYear" id="txt_ContentYear">
        <option value="">:: เลือกปีของเนื้อหา ::</option>        
        @foreach ($year as $arrYear )
            <option value="{{ $arrYear->id }}" {{ $arrYear->ContentYear==$arrContent->ContentYear ? 'selected="selected"' : '' }} > 
                {{ $arrYear->ContentYear }}
            </option>            
        @endforeach     
    </select>    
</div>

<!-- Content Status -->
<div class="form-group mt-3">     
    <label for="">เลือกสถานะรายการ</label>   
    <select class="form-control" name="txt_ContentStatus" id="txt_ContentStatus" required="required">    
        <option value="">:: เลือกสถานะรายการ ::</option>        
        <option value="on" {{ $arrContent->ContentStatus=="on" ? 'selected' : '' }} > ONLINE</option>
        <option value="off" {{ $arrContent->ContentStatus=="off" ? 'selected' : '' }}> OFFLINE</option>        
    </select>            
</div>    
@endforeach    

    </div> <!--card body-->
</div> <!--card-->

<div align="right" class="mt-1">        
    <button type="submit" class="btn btn-primary btn-x">บันทึกข้อมูล</button>
    <a href="#" class="btn btn-secondary btn-x" role="button" aria-pressed="true">ยกเลิก</a>    
</div>

</form>

        </div>
    </div>    
</div>

@endsection


