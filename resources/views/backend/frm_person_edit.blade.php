@extends('layout.master_backend')

@section('contentBackoffice')
<!-- DataTables Example -->
<div class="card mb-3 main_font">
    <div class="card-header"><i class="fas fa-table"></i>{{ $topicPage }}</div>
    <div class="card-body">
        <div class="table-responsive">

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif            

<form  method="POST" action="{{ url('manage_person/'.$id) }}" enctype="multipart/form-data">
<input type="hidden" value="{{ $id }}" name="txt_PersonID">
  @csrf  
  {{ method_field('PATCH') }}
<!-- ข้อมูลส่วนตัว -->    
<div class="card">    
    <div class="card-body">
<div>ข้อมูลส่วนตัว</div>

<!--#FETCH ARRAY-->
@foreach ($person as $arrPerson)
<!-- title name -->
<div class="form-group mt-3"> 
    <select class="form-control" name="txt_title_id">
        <option value="">:: เลือกคำนำหน้า ::</option>
        @foreach ($title_name as $arrTitleName)                                               
            <option value="{{ $arrTitleName->title_id }}" {{ $arrTitleName->title_id == $arrPerson->title_id ? 'selected="selected"' : '' }}>
                {{ $arrTitleName->title_name_th }} 
            </option>
        @endforeach
    </select>            
</div>   

<!-- name TH -->
<div class="form-group">
<div class="form-row col-md-12">
    <div class="col-md-6">
        <div class="form-label-group">
            <input type="text" name="txt_fname_th" id="firstName_th" value="{{ $arrPerson->fname_th }}" class="form-control" placeholder="ระบุชื่อ (ภาษาไทย)"  required="required">
            <label for="firstName_th">ระบุชื่อ (ภาษาไทย)</label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-label-group">
            <input type="text" name="txt_lname_th" id="lastName_th" value="{{ $arrPerson->lname_th }}" class="form-control" placeholder="ระบุนามสกุล (ภาษาไทย)" required="required">
            <label for="lastName_th">ระบุนามสกุล (ภาษาไทย)</label>
        </div>
    </div>
</div>
</div>

<!-- IDcard -->
<div class="form-group">
    <div class="form-label-group">
        <input type="text" name="txt_id_card" id="txt_id_card" value="{{ $arrPerson->id_card }}" class="form-control" placeholder="Email address" >
        <label for="txt_id_card">รหัสบัตรประจำตัวประชาชน</label>
    </div>
</div>

<!-- posAcad -->
<div class="form-group mt-3">
    <select class="form-control" name="txt_posAcad">
        <option value="">:: เลือกตำแหน่งทางวิชาการ ::</option>
        @foreach ($posAcad as $arrPosAcad)
            <option value="{{ $arrPosAcad->pos_acad_id }}" {{ $arrPosAcad->pos_acad_id == $arrPerson->pos_acac_id ? 'selected="selected"' : '' }}>
                {{ $arrPosAcad->pos_acad_name_th }}
            </option>
        @endforeach
    </select>            
</div>

<!-- posExec -->
<div class="form-group mt-3">
    <select class="form-control" name="txt_posExec">
        <option value="">:: เลือกตำแหน่งทางบริหาร ::</option>
        @foreach ($posExec as $arrPosExec)
            <option value="{{ $arrPosExec->pos_exec_id }}" {{ $arrPosExec->pos_exec_id == $arrPerson->pos_exec_id ? 'selected="selected"' : '' }}>
                {{ $arrPosExec->pos_exec_name_th }}
            </option>
        @endforeach
    </select>            
</div>

<!-- PICTURE -->
<div class="form-group">
    <label for="">ภาพบุคคล</label>
    <img src="{{asset('/images/upload/'.$arrPerson->person_picture)}}" alt="" style="height:100px">
    <input type="file" class="form-control-file" name="img" id="img" >        
</div>


    </div> <!--card body-->
</div> <!--card-->

<!-- ข้อมูลการติดต่อ -->    
<div class="card mt-1">    
    <div class="card-body">
<div>ข้อมูลที่อยู่</div>

<!-- ADDRESS -->
<div class="form-group mt-3">        
    <div class="col-md-12">
        <div class="form-label-group">
            <input type="text" name="txt_addNum" id="txt_addNum" value="{{ $arrPerson->addNum }}" class="form-control" placeholder="ที่อยู่เลขที่" >
            <label for="txt_addNum">ที่อยู่เลขที่</label>
        </div>
    </div>              
</div>   

<!-- PROVINCE AMPHUR DISTRICT -->
<div class="form-group">
<div class="form-row col-md-12">
    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_addTombol" id="txt_addTombol" value="{{ $arrPerson->addTombol }}" class="form-control" placeholder="ระบุตำบล">
            <label for="txt_addTombol">ระบุตำบล</label>
        </div>
    </div> 

    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_addAmphur" id="txt_addAmphur" value="{{ $arrPerson->addAmphur }}" class="form-control" placeholder="ระบุอำเภอ">
            <label for="txt_addAmphur">ระบุอำเภอ</label>
        </div>
    </div> 
    
    <div class="col-md-4">
    <select class="form-control" name="txt_province">
        <option value="">:: เลือกจังหวัด ::</option>
        @foreach ($province as $arrProvince)
            <option value="{{ $arrProvince->province_id }}" {{ $arrProvince->province_id == $arrPerson->addProvince ? 'selected="selected"' : '' }}>
                {{ $arrProvince->province_name }}
            </option>
        @endforeach
    </select> 
    </div>   
</div>
</div>

<!-- ZIPCODE MOBILE TEL -->
<div class="form-group">
<div class="form-row col-md-12">
    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_addZipcode" id="txt_addZipcode" value="{{ $arrPerson->addZipcode }}" class="form-control" placeholder="ระบุรหัสไปรษณีย์" >
            <label for="txt_addZipcode">ระบุรหัสไปรษณีย์</label>
        </div>
    </div> 

    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_tel" id="txt_tel" value="{{ $arrPerson->tel }}" class="form-control" placeholder="หมายเลขโทรศัพท์">
            <label for="txt_tel">หมายเลขโทรศัพท์</label>
        </div>
    </div>   

    <div class="col-md-4">
        <div class="form-label-group">
            <input type="text" name="txt_mobile" id="txt_mobile" value="{{ $arrPerson->mobile }}" class="form-control" placeholder="หมายเลขโทรศัพท์เคลื่อนที่">
            <label for="txt_mobile">หมายเลขโทรศัพท์เคลื่อนที่</label>
        </div>
    </div>         
</div>
</div>

    </div> <!--card body-->
</div> <!--card-->

<!-- ข้อมูลการดำรงตำแหน่ง -->    
<div class="card mt-1">    
    <div class="card-body">
<div>ข้อมูลการดำรงตำแหน่ง</div>

<!-- POSITION COUNCIL -->
<div class="form-group mt-3">        
    <select class="form-control" name="txt_posCounc">
        <option value="">:: เลือกตำแหน่งในสภามหาวิทยาลัย ::</option>
        @foreach ($posCouncil as $arrPoscouncil)
            <option value="{{ $arrPoscouncil->pos_counc_id }}" {{ $arrPoscouncil->pos_counc_id == $arrPerson->pos_counc_id ? 'selected="selected"' : '' }}>
                {{ $arrPoscouncil->pos_counc_name_th }}
            </option>
        @endforeach
    </select>            
</div>   

<!-- DELEGATE YEAR -->
<div class="form-group">
    <div class="form-label-group">
        <input type="text" name="txt_delegate_year" id="txt_delegate_year" value="{{ $arrPerson->delegate_year }}" class="form-control" placeholder="ปีที่รับตำแหน่ง" >
        <label for="txt_delegate_year">ปีที่รับตำแหน่ง</label>
    </div>
</div>

<!-- PERSON STATUS -->
<div class="form-group mt-3">    
    <label for="txt_Status">สถานะการดำรงตำแหน่ง</label> 
    <select class="form-control" name="txt_Status" id="txt_Status" required="required">
        <option value="">:: เลือกสถานะ ::</option>                
        <option value="on" {{ $arrPerson->person_status=="on"? "selected":"" }} > ดำรงตำแหน่ง</option>
        <option value="out"{{ $arrPerson->person_status=="on"? "":"selected" }} > หมดวาระ</option>        
    </select>            
</div>

@endforeach   <!--#FETCH ARRAY-->

    </div> <!--card body-->
</div> <!--card-->


<div align="right" class="mt-1">        
    <button type="submit" class="btn btn-primary btn-x">บันทึกข้อมูล</button>
    <a href="#" class="btn btn-secondary btn-x" role="button" aria-pressed="true">ยกเลิก</a>    
</div>

</form>

        </div>
    </div>    
</div>

@endsection