@extends('layout.master_backend')

@section('contentBackoffice')
{{-- MAIN Backoffice --}}
<div class="col-12 main_font">
    <div class="card">
        <div class="card-header card_bg_topic">USER MANAGEMENT</div>
        <div class="card-body">           
        {{-- CONTENT --}}
        <div class="row">
            <div class="col-4">
                    <img class="card-img-top mt-2" src="http://placehold.it/380x180" alt="">
            </div>
            <div class="col-8">
                {{-- NAME --}}
                <div class="row mt-2">
                    <div class="col-3"><b>NAME : </b></div>
                    <div class="col-9">THITINAN PHUPHANT</div>
                </div>{{-- NAME --}}

                {{-- USER NAME --}}
                <div class="row mt-2">
                    <div class="col-3"><b>USER NAME : </b></div>
                    <div class="col-9">thitinan.p</div>
                </div>{{-- USER NAME --}}

                {{-- PASSWORD --}}
                <div class="row mt-2">
                    <div class="col-3"><b>PASSWORD : </b></div>
                    <div class="col-9">************* <a href="#">Change</a></div>
                </div>{{-- PASSWORD --}}

                {{-- PERMISSION --}}
                <div class="row mt-2">
                    <div class="col-3"><b>PERMISSION : </b></div>
                    <div class="col-9">Administrator</div>
                </div>{{-- PERMISSION --}}
            </div>
        </div>
        {{-- CONTENT --}}
        </div>
    </div>
</div>{{-- MAIN Backoffice --}}
@endsection