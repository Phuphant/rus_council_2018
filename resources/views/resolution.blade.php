@extends('layout.master')

@section('content')
<!-- Page Content --> 
<div class="container main_font">
  <div class="row ">
    {{-- $category --}}
    {{-- MENU LEFT --}}
    <div class="col-3">      
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      @foreach ($category as $arrCategory)
          <a class="nav-link" id="v-pills-{{ $arrCategory->CategoryCode }}-tab" data-toggle="pill" href="#v-pills-{{ $arrCategory->CategoryCode }}" role="tab" aria-controls="v-pills-{{ $arrCategory->CategoryCode }}">
            {{ $arrCategory->CategoryName }}
          </a>
      @endforeach        
      </div>
    </div>

    {{-- CONTENT RIGHT --}}
    <div class="col-9">
        @include('subView.show_resolution')
    </div>
    
    {{-- DIV ROW CONTAINER --}}
  </div>
</div>
@endsection

