@extends('layout.master')

@section('content')
<!-- Page Content --> 
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="card pl-2 ">
        {{ $DataYear }}
        {{-- TAB --}}        
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pills-2557-tab" data-toggle="pill" href="#pills-2557" role="tab" aria-controls="pills-2557" aria-selected="true">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
            </li>
          </ul>

          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-2557" role="tabpanel" aria-labelledby="pills-2557-tab">..1..</div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">..2..</div>
            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">..3...</div>
          </div>

      </div>      
    </div>

    <!-- DIV row - container -->
  </div>
</div>
@endsection

