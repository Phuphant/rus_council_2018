@extends('layout.master')

@section('content')
<!-- Page Content -->  
<div class="container">
  <div class="row">
    <div class="col-sm-8">

      {{-- HILIGHT --}}
      <div class="row"> {{-- CLASS row hilight--}}
          <div class="col-12">
            <div class="card mb-2">
            <h4 class="main_font ml-1 mt-3">HILIGHT</h4>
              @foreach ($Content_Show_Hilight as $arrContentHilight )
                <div class="card pl-2 mb-1 mt-1 ml-1 mr-1">
                  <h4 class="mt-4 main_font">
                    {{ $arrContentHilight->ContentName }}
                      <!--<img class="img-fluid" style="padding-top:10px;" width="10%" src="{{URL::asset('/images/icon_new.gif')}}">-->
                  </h4>
                  {{ $arrContentHilight->ContentDetail }}              
                </div>
              @endforeach  
            </div>  
          </div>         
      </div> {{-- CLASS row hilight--}}

      {{-- NEWS --}}
      <div class="row"> {{-- CLASS row news MAIN--}}                     
        <div class="col-12">
          <div class="card mb-2">
            <h4 class="main_font ml-1 mt-3">NEWS</h4>
            <div class="row"> {{-- CLASS row news in card--}}   
            @foreach ($Content_Showall as $arrContentAll)
              <div class="col-6">
                <div class="card pl-1 pr-1 mb-1 mt-1 ml-1 mr-1">
                  <img class="card-img-top mt-2" src="http://placehold.it/380x180" alt="">
                  <h5 class="main_font">
                  <a href="#">{{ $arrContentAll->ContentName }}</a>
                  </h5>              
                </div>
              </div>    
            @endforeach
            </div> {{-- CLASS row news in card --}}   
          </div>
        </div>
      </div> {{-- CLASS row news MAIN--}}
                
    {{-- col-sm-8--}}
    </div>

    <div class="col-sm-4 main_font">
      <div class="card mb-1 ">
        <div class="card-header card_bg_topic">นายกสภามหาวิทยาลัย</div>
        <div class="card-body" align="center">
          <img class="card-img-top" src="http://placehold.it/300x200" alt="">
          @foreach ($president_of_council as $arrPresident )
              {{ $arrPresident->title_name_th.$arrPresident->fname_th }}  
              {{ $arrPresident->lname_th }}
          @endforeach
        </div>
      </div>
      <div class="card mb-1">
        <div class="card-header card_bg_topic">รายงานผลการดำเนินงาน</div>
        <div class="card-body">
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Cras justo odio</li>
            <li class="list-group-item">Dapibus ac facilisis in</li>
            <li class="list-group-item">Vestibulum at eros</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

