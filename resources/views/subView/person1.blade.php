@foreach ($posCouncil as $arrposCouncil)    
    <div class="card pl-2 mb-1 ">
        <h5 class="mt-4 main_font" align="center">             
            {{ $arrposCouncil->pos_counc_name_th }}            
            {{ $arrposCouncil->pos_counc_id }}            
        </h5> 
        
        @php
        switch($arrposCouncil->pos_counc_id){
            case '1' : $showCol = "col-12";     break;
            case '2' : $showCol = "col-12";     break;
            case '3' : $showCol = "col-6";      break;
            case '4' : $showCol = "col-4";      break;
            case '5' : $showCol = "col-4";      break;
            case '6' : $showCol = "col-4";      break;
            case '7' : $showCol = "col-12";     break;
        }
        @endphp
        
        <div class="row col-12">
        @foreach ($DataHisPerson as $arrHisPerson)        
            @if ($arrposCouncil->pos_counc_id == $arrHisPerson->pos_counc_id)   
                <div class='card {{ $showCol }}'>
                <p class="main_font" align="center">                    
                    <img class="img-fluid" style="padding-top:10px;" width="10%" src="{{URL::asset('/images/icon_new.gif')}}">
                    <div align="center">
                        {{ $arrHisPerson->title_name_th.$arrHisPerson->fname_th }}
                        {{ $arrHisPerson->lname_th }}
                    </div>
                </p>    
                </div>                         
            @endif
        @endforeach
        </div>
        
    </div>    
@endforeach